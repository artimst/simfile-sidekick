TELEGR_START = """
    Hello! My name is Simfile Sidekick! I'm a bot that can help you locate StepMania simfile information. Type `/help`
    to get a list of commands and information on how to use them.
"""

TELEGR_HELP_1 = """
    Hello! I'm Simfile Sidekick, a bot inspired by Nav's Breakdown Buddy that can search, parse, and display information
    about StepMania simfiles.
"""

TELEGR_HELP_2 = """
    I can currently parse StepMania files using a library of popular packs. Use `/search` followed by the song name
    ```Example /search Black Lamp```
"""

TELEGR_HELP_3 = """
    I can also search by tags. Currently, the supported tags are `title`, `subtitle`, `artist`, `stepartist`, `rating`,
    and `bpm`. Tags must be prefixed with `-`. Multiple tags can be used at once. A song name _doesn't_ have to be
    provided:
    ```Example /search Sigatrev -rating:20``` ```Example /search -artist:Pendulum -bpm:175 -stepartist:Archi```
"""

TELEGR_HELP_4 = """
    If you want me to parse a .sm file, send the .sm file as an attachment and type `/parse` for the caption. If I get
    stuck parsing a file, you can try and run `/fix` and I'll do my best to cleanup in order to parse files again for
    you.
"""

TELEGR_HELP_5 = """
    Note that if this bot is being used in a group chat, you might first have to use the start command and tag the bot
    name: `/start@SimfileSidekickBot`.
"""

KEY_TITLE = "title"
KEY_SUBTITLE = "subtitle"
KEY_ARTIST = "artist"
KEY_PACK = "pack"
KEY_RATING = "rating"
KEY_DIFFICULTY = "difficulty"
KEY_STEPARTIST = "stepartist"
KEY_GRAPH = "graph_location"
KEY_BREAKDOWN = "breakdown"
KEY_PARTIAL_BD = "partial_breakdown"
KEY_SIMPLE_BD = "simple_breakdown"
KEY_NORM_BD = "normalized_breakdown"
KEY_COLOR_BD = "color_breakdown"
KEY_COLOR_PARTIAL_BD = "color_partial_breakdown"
KEY_COLOR_SIMPLE_BD = "color_simple_breakdown"
KEY_COLOR_NORM_BD = "color_normalized_breakdown"
KEY_GRAPH_COLOR_BD = "joined_graph_and_color_bd"


TELEGR_TOKEN_KEY = "TELEGRAM_TOKEN"

RGX_CONSEC_WS = r"\s{2,}"

MARKDOWNV2 = "MarkdownV2"

PHONE_ARROW = U'\U0001F4F2'
HOURGLASS = u'\u23F3'
CHECKMARK = u'\u2705'

EMPTY_STR = ""
SPACE = ' '
NL = '\n'
DBL_NL = "\n\n"


NOT_AVAIL = "N/A"




DB_NAME = "db.json"  # Name of the TinyDB database file that contains parsed song information



# By using context.user_data in any Handler callback, you have access to a user-specific dict.
SONG_SEARCH_DATA = "data"
SONG_SEARCH_NUM_RESULTS = "max_val"
