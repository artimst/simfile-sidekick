import re

from common import Constants as const

def md_esc_all(txt: str) -> str:
    """
    Properly escapes all special characters in a given text with a backslash. The special characters are:
        '_', '*', '[', ']', '(', ')', '~', '`', '>', '#', '+', '-', '=', '|', '{', '}', '.', and '!'
    These are the special characters that need to be escaped when printing a message in Telegram using the MarkdownV2
    text format. Python's re.escape() takes care of most these characters, but the remainder need to be manually
    replaced.

    @param txt: The text to escape.
    @return: The same text with all special characters escaped with a backslash.
    """
    return re.escape(txt)\
        .replace("_", "\_")\
        .replace("`", "\`")\
        .replace(">", "\>")\
        .replace("=", "\=")\
        .replace("!", "\!")


def md_esc_punc(txt: str) -> str:
    """
    Properly escapes all punctuation characters in a given text with a backslash. Primarily used for messages sent in
    Telegram using the MarkdownV2 text format as periods and exclamation points are special characters that need to be
    escaped.

    @param txt: The text to escape.
    @return: The same text with periods escaped with a backslash.
    """
    return txt.replace(".", "\.")\
        .replace("!", "\!")


def fmt_multiline(txt: str) -> str:
    """
    Formats Python multi-line strings to remove excess whitespace characters. Any text that contains any instance of
    two or more consecutive whitespace characters will be replaced with a space.

    @param txt: The text to format.
    @return: The same text with excess whitespace characters removed.
    """
    return re.sub(const.RGX_CONSEC_WS, const.SPACE, txt.strip())


def telegr_mono_desc(mono_pct: float) -> str:
    """
    Helper function to return pre-formatted text used in the mono pattern analysis for Telegram.

    @param mono_pct: Percentage of "mono" in a simfile.
    @return: The pre-formatted description.
    """
    if mono_pct == 0:
        return "_None_"
    elif mono_pct > 0 and mono_pct < 10:
        return "_Sparce_"
    elif mono_pct >= 10 and mono_pct < 30:
        return "Infrequent"
    elif mono_pct >= 30 and mono_pct < 50:
        return "*Moderate*"
    elif mono_pct > 50:
        return "*__Repetitive__*"
    else:
        # We shouldn't get here since mono_pct is typically a value between 0 and 100 inclusively.
        return const.EMPTY_STR


def round_float(num: float) -> str:
    """
    Return a string representation of a float rounded to two decimal places.

    @param num: The float to round.
    @return: A string representation of the input float, rounded to two decimal places.
    """
    return "{:.2f}".format(float(num))