# -*- coding: utf-8 -*-

"""
sstgbot.py - Simfile Sidekick Telegram Bot

Searches a TinyDB database created by scan.py and reports information back to a Telegram user.

This program is to be used in conjunction with scan.py. It will scan the database created by scan.py and allow users
to search this database through a Telegram interface. Also allows users to upload their own data and have the scanner
parse through the uploaded file.

This is free and unencumbered software released into the public domain. For more information, please refer to the
LICENSE file or visit <https://unlicense.org>.

Created with love by Artimst for the Dickson City Heroes and Stamina Nation.
"""

import logging
import os
import re
import shutil
import sys
import traceback
import telegram.error
import tinydb


from common import DBManager as dbm
from common import Constants as const
from common import Common as cmn
from dotenv import load_dotenv
from telegram import Update
from telegram.ext import filters, ApplicationBuilder, ContextTypes, CommandHandler, MessageHandler, ConversationHandler
from tinydb import Query, TinyDB, where
from scan import parse_file, scan_folder


SELECT = range(1)

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)


def get_song_details(data: type[tinydb.table.Document]) -> str:
    song_details = "*__" + "Song Details" + "__*" + "\n"

    # Title, Subtitle, and Artist
    if data["title"] == "*Hidden*" and data["artist"] == "*Hidden*":
        song_details += "_" + cmn.md_esc_all("<Title and artist hidden>") + "_" + "\n"
    else:
        song_details += "*" + cmn.md_esc_all(data["title"]) + "* "
        if data["subtitle"] and data["subtitle"] != "N/A":
            song_details += "*_" + cmn.md_esc_all(data["subtitle"]) + "_* "
        song_details += "by *" + cmn.md_esc_all(data["artist"]) + "*" + "\n"

    # Pack(s), Rating, Difficulty, and Stepartist
    song_details += cmn.md_esc_all("From pack(s): ") + cmn.md_esc_all(data["pack"]) + "\n"
    song_details += "*" + data["rating"] + "* "
    song_details += data["difficulty"] + " \- " + cmn.md_esc_all(data["stepartist"]) + "\n\n"

    # Song Length
    song_details += "__Song Length:__ " + data["length"] + "\n"

    # Display BPM
    if data["display_bpm"] and data["display_bpm"] != "N/A":
        song_details += "__Display BPM:__ "
        if re.search(r"[:]+", data["display_bpm"]):
            disp_bpm_range = data["display_bpm"].split(":")
            song_details += str(int(float(disp_bpm_range[0]))) + "\-"
            song_details += str(int(float(disp_bpm_range[1]))) + "\n"
        else:
            song_details += str(int(float(data["display_bpm"]))) + "\n"

    # BPM
    song_details += "__BPM:__ "
    if int(float(data["min_bpm"])) == int(float(data["max_bpm"])):
        song_details += str(int(float(data["min_bpm"]))) + "\n"
    else:
        song_details += str(int(float(data["min_bpm"]))) + "\-"
        song_details += str(int(float(data["max_bpm"]))) + "\n"

    # NPS
    song_details += "__Peak NPS:__ *" + cmn.md_esc_all(cmn.round_float(data["max_nps"])) + "* notes/s\." + "\n"
    song_details += "__Median NPS:__ *" + cmn.md_esc_all(cmn.round_float(data["median_nps"])) + "* notes/s\." + "\n"

    # Total Stream/Break
    total_measures = data["total_stream"] + data["total_break"]
    if total_measures != 0:
        stream_percent = cmn.round_float((data["total_stream"] / total_measures) * 100)
        break_percent = cmn.round_float((data["total_break"] / total_measures) * 100)
        song_details += "__Total Stream:__ *" + str(data["total_stream"]) + "* measures "
        song_details += "\(" + cmn.md_esc_all(stream_percent) + "%\)" + "\n"
        song_details += "__Total Break:__ *" + str(data["total_break"]) + "* measures "
        song_details += "\(" + cmn.md_esc_all(break_percent) + "%\)"

    return song_details


def get_chart_info(data: type[tinydb.table.Document]) -> str:
    chart_info = "*__" + "Chart Info" + "__*" + "\n"
    chart_info += "__Notes:__ " + str(data["notes"]) + "\n"
    chart_info += "__Jumps:__ " + str(data["jumps"]) + "\n"
    chart_info += "__Holds:__ " + str(data["holds"]) + "\n"
    chart_info += "__Mines:__ " + str(data["mines"]) + "\n"
    chart_info += "__Hands:__ " + str(data["hands"]) + "\n"
    chart_info += "__Rolls:__ " + str(data["rolls"])
    return chart_info


def get_pattern_analysis(data: type[tinydb.table.Document]) -> str:
    pattern_analysis = "*__" + "Pattern Analysis" + "__*" + "\n"
    pattern_analysis += "_Analysis does not consider patterns in break segments\._" + "\n"

    # Candles
    pattern_analysis += "__Candles:__ **" + str(data["total_candles"]) + "** "
    pattern_analysis += "\(" + str(data["left_foot_candles"]) + " left, "
    pattern_analysis += str(data["right_foot_candles"]) + " right\)" + "\n"

    candle_density = data["total_candles"] / (data["notes"] / 16)
    pattern_analysis += "__Candle density:__ " + cmn.md_esc_all(str(cmn.round_float(candle_density))) + " candles/measure" + "\n"

    # Mono
    pattern_analysis += "__Mono:__ " + cmn.md_esc_all(str(cmn.round_float(data["mono_percent"]))) + "% "
    pattern_analysis += "\(" + cmn.telegr_mono_desc(data["mono_percent"]) + "\)" + "\n"

    # Boxes
    corner_boxes = data["corner_ld_boxes"] + data["corner_lu_boxes"] + data["corner_rd_boxes"] + data["corner_ru_boxes"]
    total_boxes = data["lr_boxes"] + data["ud_boxes"] + corner_boxes
    pattern_analysis += "__Boxes:__ *" + str(total_boxes) + "* "
    pattern_analysis += "\(" + str(data["lr_boxes"]) + " LRLR, " + str(data["ud_boxes"]) + " UDUD, "
    pattern_analysis += str(corner_boxes) + " corner\)" + "\n"

    # Anchors
    total_anchors = data["anchor_left"] + data["anchor_down"] + data["anchor_up"] + data["anchor_right"]
    pattern_analysis += "__Anchors:__ *" + str(total_anchors) + "* "
    pattern_analysis += "\(" + str(data["anchor_left"]) + " left, "
    pattern_analysis += str(data["anchor_down"]) + " down, "
    pattern_analysis += str(data["anchor_up"]) + " up, "
    pattern_analysis += str(data["anchor_right"]) + " right" + "\)"
    return pattern_analysis


def get_breakdown_info(result: type[tinydb.table.Document]) -> str:
    """
    Creates a pre-formatted string to be sent in Telegram that contains breakdown information of a users selected song.

    @param result: Simfile information of a users selected song
    @return: A pre-formatted string containing breakdown information of a users selected song.
    """
    msg = const.EMPTY_STR

    if result[const.KEY_BREAKDOWN]:
        msg += "*__" + "Detailed Breakdown" + "__*" + const.NL
        msg += "`" + cmn.md_esc_all(result[const.KEY_BREAKDOWN]) + "`" + const.DBL_NL

    # For partially simplified, simplified, and normalized breakdowns, any condensed 24th run already had an asterisk
    # that is double escaped. Since md_esc_all() will escape all special characters again, we need to remove one of
    # these backslashes; this is what the .replace("\*", "*") is for.

    if result[const.KEY_PARTIAL_BD] and (result[const.KEY_PARTIAL_BD] != result[const.KEY_SIMPLE_BD]):
        msg += "*__" + "Partially Simplified" + "__*" + const.NL
        msg += "`" + cmn.md_esc_all(result[const.KEY_PARTIAL_BD].replace("\*", "*")) + "`" + const.DBL_NL

    if result[const.KEY_SIMPLE_BD]:
        msg += "*__" + "Simplified Breakdown" + "__*" + const.NL
        msg += "`" + cmn.md_esc_all(result[const.KEY_SIMPLE_BD].replace("\*", "*")) + "`" + const.DBL_NL

    if result[const.KEY_NORM_BD]:
        msg += "*__" + "Normalized Breakdown" + "__*" + const.NL
        msg += "_This is in beta and may be inaccurate\. Variable BPM songs may report incorrect BPM\._" + const.NL
        msg += "`" + cmn.md_esc_all(result[const.KEY_NORM_BD].replace("\*", "*")) + "`"

    return msg


async def send_song_info(update: Update, context: ContextTypes.DEFAULT_TYPE, result: type[tinydb.table.Document]):
    """
    Function that sends simfile information to the user. Prints out 5 separate messages that contain: song details,
    chart info, pattern analysis, breakdown information, and a screenshot of the density graph.

    @param update: A class provided by python-telegram-bot that fetches updates from Telegram
    @param context: A class provided by python-telegram-bot that allows us to access to commonly used objects
    @param result: Simfile information of a users selected song
    @return: Returns nothing.
    """
    await context.bot.send_message(
        chat_id=update.effective_chat.id, text=get_song_details(result), parse_mode=const.MARKDOWNV2
    )
    await context.bot.send_message(
        chat_id=update.effective_chat.id, text=get_chart_info(result), parse_mode=const.MARKDOWNV2
    )
    await context.bot.send_message(
        chat_id=update.effective_chat.id, text=get_pattern_analysis(result), parse_mode=const.MARKDOWNV2
    )

    # It's possible get_breakdown_info() will return an empty string for some beginner charts without any stream, so
    # conditionally check if this field gets populated.
    msg = get_breakdown_info(result)
    if msg:
        await context.bot.send_message(chat_id=update.effective_chat.id, text=msg, parse_mode=const.MARKDOWNV2)

    await context.bot.send_photo(chat_id=update.effective_chat.id, photo=open(result[const.KEY_GRAPH], "rb"))


# Functions below define all Simfile Sidekick Telegram commands


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """
    The /start command of the bot. Prints a small welcome message to the user.

    @param update: A class provided by python-telegram-bot that fetches updates from Telegram
    @param context: A class provided by python-telegram-bot that allows us to access to commonly used objects
    @return: Returns nothing.
    """
    msg = cmn.fmt_multiline(cmn.md_esc_punc(const.TELEGR_START))
    await context.bot.send_message(chat_id=update.effective_chat.id, text=msg, parse_mode=const.MARKDOWNV2)


async def help(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """
    The /help command of the bot. Prints all the supported commands and how to use them.

    @param update: A class provided by python-telegram-bot that fetches updates from Telegram
    @param context: A class provided by python-telegram-bot that allows us to access to commonly used objects
    @return: Returns nothing.
    """
    msg = cmn.fmt_multiline(cmn.md_esc_punc(const.TELEGR_HELP_1))
    msg += const.DBL_NL + cmn.fmt_multiline(cmn.md_esc_punc(const.TELEGR_HELP_2))
    msg += const.DBL_NL + cmn.fmt_multiline(cmn.md_esc_punc(const.TELEGR_HELP_3))
    msg += const.DBL_NL + cmn.fmt_multiline(cmn.md_esc_punc(const.TELEGR_HELP_4))
    msg += const.DBL_NL + cmn.fmt_multiline(cmn.md_esc_punc(const.TELEGR_HELP_5))
    await context.bot.send_message(chat_id=update.effective_chat.id, text=msg, parse_mode=const.MARKDOWNV2)


async def search(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """
    The /search command of the bot. Prints out simfile information for a searched song. If the users search criteria
    returns multiple matches, instead outputs a list of matches to the user and allows them to select one (see the
    select() function).

    @param update: A class provided by python-telegram-bot that fetches updates from Telegram
    @param context: A class provided by python-telegram-bot that allows us to access to commonly used objects
    @return: Returns nothing.
    """
    user = str(update.message.from_user.username)
    user_input = const.SPACE.join(context.args)

    if not user_input:
        msg = f"Sorry {user}, but please supply a title."
        await context.bot.send_message(chat_id=update.effective_chat.id, text=msg)
        return

    results = dbm.search(user_input.strip(), const.DB_NAME)

    if isinstance(results, int):
        if results == 0:
            msg = f"Sorry {user}, but I could not find any songs."
            await context.bot.send_message(chat_id=update.effective_chat.id, text=msg)
        else:
            # Normally -1 is returned if our database manager has an internal error
            msg = f"Sorry {user}, there was an error processing this request. Please try again later."
            await context.bot.send_message(chat_id=update.effective_chat.id, text=msg)
    elif isinstance(results, list):
        if len(results) == 1:
            await send_song_info(update, context, results[0])
        elif len(results) > 1:
            max_val = len(results)
            msg = "*__Search Results__*" + const.DBL_NL

            if max_val > 25:
                max_val = 25
                msg += cmn.md_esc_all(cmn.fmt_multiline(f"""
                    There were too many results, I can only show you the first 25. If your song isn't listed, please
                    refine your search. {user}, enter a number from 1 to {max_val} to select the search result.
                """.strip())) + const.DBL_NL
            else:
                msg += cmn.md_esc_all(cmn.fmt_multiline(f"""
                    {user}, enter a number from 1 to {max_val} to select the search result.
                """.strip())) + const.DBL_NL

            for i, result in enumerate(results):
                if i >= max_val:
                    break

                msg += ">`" + str(i + 1) + "`" + (4 * const.SPACE)
                msg += "*" + cmn.md_esc_all(result[const.KEY_TITLE]) + "* "
                if result[const.KEY_SUBTITLE] and result[const.KEY_SUBTITLE] != const.NOT_AVAIL:
                    msg += "_" + cmn.md_esc_all(result[const.KEY_SUBTITLE]) + "_ "
                msg += "by " + cmn.md_esc_all(result[const.KEY_ARTIST]) + const.NL
                msg += "Pack\(s\): " + cmn.md_esc_all(result[const.KEY_PACK]) + const.NL
                msg += "*" + result[const.KEY_RATING] + "* "
                msg += result[const.KEY_DIFFICULTY] + " \- " + cmn.md_esc_all(result[const.KEY_STEPARTIST])
                if i != max_val:
                    msg += const.DBL_NL

            await context.bot.send_message(chat_id=update.effective_chat.id, text=msg, parse_mode=const.MARKDOWNV2)
            # Store these values so when the code advances to the select() function, we can retrieve them
            context.user_data[const.SONG_SEARCH_DATA] = results
            context.user_data[const.SONG_SEARCH_NUM_RESULTS] = max_val
            # Tells the ConversationHandler that the next action is the select() function
            return SELECT


async def select(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """
    This function is called after a user selects a song when a search provides multiple choices. After a user selects a
    song, this function will send the chart data to the user.

    @param update: A class provided by python-telegram-bot that fetches updates from Telegram
    @param context: A class provided by python-telegram-bot that allows us to access to commonly used objects
    @return: Returns nothing.
    """
    user = str(update.message.from_user.username)
    user_input = update.message.text

    # Retrieves information that was saved when the user first searched for a song
    results = context.user_data[const.SONG_SEARCH_DATA]
    max_val = context.user_data[const.SONG_SEARCH_NUM_RESULTS]

    try:
        user_input = int(user_input)
    except ValueError:
        msg = f"Sorry {user}, but that option is invalid."
        await context.bot.send_message(chat_id=update.effective_chat.id, text=msg)
        return ConversationHandler.END

    if user_input > max_val or user_input < 1:
        msg = f"Sorry {user}, but that option is invalid."
        await context.bot.send_message(chat_id=update.effective_chat.id, text=msg)
        return ConversationHandler.END

    await send_song_info(update, context, results[user_input - 1])
    return ConversationHandler.END


async def parse(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """
    The /parse command of the bot. This function is only called when a user uploads a .sm file with the caption of
    "/parse". Prints out simfile information of an uploaded song.

    @param update: A class provided by python-telegram-bot that fetches updates from Telegram
    @param context: A class provided by python-telegram-bot that allows us to access to commonly used objects
    @return: Returns nothing.
    """
    user = cmn.md_esc_all(str(update.message.from_user.username))
    user_id = str(update.message.from_user.id)
    file_name = update.message.document.file_name

    usr_tmp_dir = "./tmp/telegram/" + user_id + "/"
    usr_tmp_file = usr_tmp_dir + file_name
    usr_tmp_db = usr_tmp_file + ".json"

    try:
        os.makedirs(usr_tmp_dir)
    except FileExistsError:
        msg = f"It looks like I'm already parsing a file for you {user}"
        await context.bot.send_message(chat_id=update.effective_chat.id, text=msg)
        return

    msg = f"{const.PHONE_ARROW} {cmn.md_esc_all(user)}, I'm downloading your file `{file_name}` \.\.\."
    msg_obj = await context.bot.send_message(chat_id=update.effective_chat.id, text=msg, parse_mode=const.MARKDOWNV2)

    new_file = await update.message.effective_attachment.get_file()
    await new_file.download_to_drive(usr_tmp_file)

    msg = f"{const.HOURGLASS} {cmn.md_esc_all(user)}, finished downloading; parsing `{file_name}` \.\.\."
    await context.bot.edit_message_text(chat_id=update.effective_chat.id, message_id=msg_obj.message_id,
        text=msg, parse_mode=const.MARKDOWNV2)

    db = TinyDB(usr_tmp_db)

    parse_file(db, usr_tmp_file, usr_tmp_dir, "Uploaded", hide_artist_info=False, cache=None)

    results = [result for result in db]

    msg = f"{const.CHECKMARK} {cmn.md_esc_all(user)}, finished parsing\!"
    await context.bot.edit_message_text(chat_id=update.effective_chat.id, message_id=msg_obj.message_id,
        text=msg, parse_mode=const.MARKDOWNV2)

    for result in results:
        await send_song_info(update, context, result)
        if os.path.exists(result[const.KEY_GRAPH]):
            os.remove(result[const.KEY_GRAPH])
        if os.path.exists(result[const.KEY_COLOR_BD]):
            os.remove(result[const.KEY_COLOR_BD])
        if os.path.exists(result[const.KEY_COLOR_PARTIAL_BD]):
            os.remove(result[const.KEY_COLOR_PARTIAL_BD])
        if os.path.exists(result[const.KEY_COLOR_SIMPLE_BD]):
            os.remove(result[const.KEY_COLOR_SIMPLE_BD])
        if os.path.exists(result[const.KEY_COLOR_NORM_BD]):
            os.remove(result[const.KEY_COLOR_NORM_BD])
        if os.path.exists(result[const.KEY_GRAPH_COLOR_BD]):
            os.remove(result[const.KEY_GRAPH_COLOR_BD])

    os.remove(usr_tmp_file)
    db.close()
    os.remove(usr_tmp_db)
    os.rmdir(usr_tmp_dir)


async def fix(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """
    The /fix command of the bot. This will clean up the users temp directory for parsing files. The /parse command won't
    parse an uploaded .sm file if that users temp directory has files in it. Normally the /parse command cleans these
    files up afterward, but if there's an exception thrown or the program encounters an error, it may not properly clean
    up this temp directory.

    @param update: A class provided by python-telegram-bot that fetches updates from Telegram
    @param context: A class provided by python-telegram-bot that allows us to access to commonly used objects
    @return: Returns nothing.
    """
    user = str(update.message.from_user.username)
    user_id = str(update.message.from_user.id)
    usr_tmp_dir = "./tmp/telegram/" + user_id + "/"

    if os.path.exists(usr_tmp_dir):
        shutil.rmtree(usr_tmp_dir)
        msg = f"I did some cleanup {user}, I should be able to parse files again for you!"
        await context.bot.send_message(chat_id=update.effective_chat.id, text=msg)
    else:
        msg = f"{user}, it looks like there's nothing for me to cleanup."
        await context.bot.send_message(chat_id=update.effective_chat.id, text=msg)


if __name__ == "__main__":
    load_dotenv()
    token = os.getenv(const.TELEGR_TOKEN_KEY)

    if not token:
        msg = """
            It looks like you don't have an ".env" file, or it's not setup correctly. Please make sure you have an
            ".env" file in the same directory as this script. The ".env" will need to have a line that contains your 
            Telegram token: "TELEGRAM_TOKEN=YourBotsTelegramTokenHere" ... see 
            https://core.telegram.org/bots/api#authorizing-your-bot for more information on how to generate and use a
            token. If you do have an ".env" file with your token included, make sure this script has read access to the
            ".env" file.
        """
        msg = re.sub(const.RGX_CONSEC_WS, " ", msg.strip())
        print(msg)
        sys.exit(1)

    application = ApplicationBuilder().token(token).build()

    start_handler = CommandHandler("start", start)
    application.add_handler(start_handler)

    help_handler = CommandHandler("help", help)
    application.add_handler(help_handler)

    search_handler = ConversationHandler(
        entry_points=[CommandHandler("search", search)],
        fallbacks=[],
        states={
            SELECT: [MessageHandler(filters.TEXT, select)]
        },
    )
    application.add_handler(search_handler)

    parse_handler = MessageHandler(filters.Document.FileExtension("sm") & filters.Caption("/parse"), parse)
    application.add_handler(parse_handler)

    fix_handler = CommandHandler("fix", fix)
    application.add_handler(fix_handler)

    try:
        application.run_polling()
    except telegram.error.InvalidToken:
        print(traceback.format_exc())
        msg = """
            The Telegram server rejected your token. Check to make sure your token is defined correctly in your ".env"
            file, and that it is copy and pasted properly. 
        """
        msg = re.sub(const.RGX_CONSEC_WS, " ", msg.strip())
        print(msg)
        sys.exit(1)
